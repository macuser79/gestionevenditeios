//
//  UIColor+Button.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 19/04/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit

extension UIButton {
    
    func addCornerRadius(){
        self.layer.cornerRadius = 5.0;
    }
}
