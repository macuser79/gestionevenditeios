//
//  UIColor+Textfield.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 12/03/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit

extension UITextField {
    
    func addBottomBorder(){
        
        let bottomBorder = UIView();
        bottomBorder.backgroundColor = UIColor.white
        bottomBorder.frame = CGRect(x: 0, y: self.frame.height - 1, width: self.frame.width, height:1);
        self.addSubview(bottomBorder);
    }
    
    func clearBackground(){
        self.backgroundColor = UIColor.clear;
    }
    
    
    func cornerRadius(){
        self.layer.cornerRadius = 5.0;
    }
    
}
