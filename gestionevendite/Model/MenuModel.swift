//
//  MenuModel.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 09/03/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit

class MenuModel: NSObject {
    
    var idMenu : Int?
    var imageMenu : String?
    var titleMenu : String?
    
    
    init(idMenu: Int, imageMenu: String, titleMenu: String) {
        self.idMenu = idMenu;
        self.imageMenu = imageMenu;
        self.titleMenu = titleMenu;
    }
}
