//
//  Client.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 20/04/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import Foundation
import RealmSwift

class Client: Object {
    
    @objc dynamic var cap = "";
    @objc dynamic var citta = "";
    @objc dynamic var clienteId = 0;
    @objc dynamic var email = "";
    @objc dynamic var fax = "";
    @objc dynamic var flgAcconto = 0;
    @objc dynamic var idListino = 0;
    @objc dynamic var identificativo = "";
    @objc dynamic var indirizzo = "";
    @objc dynamic var note = "";
    @objc dynamic var orario = "";
    @objc dynamic var piva = "";
    @objc dynamic var provincia = "";
    @objc dynamic var ragioneSociale = "";
    @objc dynamic var rivendita = "";
    @objc dynamic var telefono = "";
}


