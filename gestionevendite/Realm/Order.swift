//
//  Order.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 19/11/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit

class Order: NSObject {

    var id: String = "";
    var idOrder: String = "";
    var idClient: String = "";
    var descClient: String = "";
    var descOrder:String = "";
    var idStato:String="";
    var descStato:String = "";
    var quantita:String = "";
    var prezzo:String = "";
    var dataOrdine:String = "";
}
