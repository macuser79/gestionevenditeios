//
//  ViewController.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 26/02/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit
import Alamofire
import LocalAuthentication
import APESuperHUD
import SwiftyJSON

class LoginViewController: UIViewController {
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var buttonLogin: UIButton!
    
    let defaultValues = UserDefaults.standard;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if defaultValues.string(forKey: "username") != nil{
             let welcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "welcomevc") as! WelcomeViewController;
            self.navigationController?.pushViewController(welcomeViewController, animated: true)
            
        }
        let backButton = UIBarButtonItem(title: "", style: .plain,
                                         target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton;
        txtUsername.addBottomBorder();
        txtPassword.addBottomBorder();
        self.hideKeyboardWhenTappedAround();
        let colorNav = UIColor.init(red: 60.0/255.0, green: 99.0/255.0, blue: 126.0/255.0, alpha: 1.0);
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = colorNav
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = titleDict as! [NSAttributedString.Key : Any]
        
        
        
    }

    
    @IBAction func loginUser(_ sender: Any) {
        
        let hudViewController = APESuperHUD(style: .loadingIndicator(type: .standard), title: nil, message: "Loading...")
        present(hudViewController, animated: true)
        let encryptPassword = SHA1.hexString(from: txtPassword.text!)?.removeWhitespace().lowercased();
        Alamofire.SessionManager.default.request(UserRouter.loginUser(username: txtUsername.text!, password: encryptPassword!)).responseJSON{
                response in
            //print(response.request)
            //print(response.response)
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            if let JSONData = response.result.value{
                let jsonData = JSON(JSONData);
                self.defaultValues.set(jsonData["username"].stringValue, forKey: "username");
                self.defaultValues.setValue(jsonData["nome"].stringValue, forKey: "nome");
                self.defaultValues.setValue(jsonData["cognome"].stringValue, forKey: "cognome");
                self.defaultValues.setValue(jsonData["denominazione"].stringValue, forKey: "denominazione");
                
                hudViewController.dismiss(animated: true);
                let welcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "welcomevc") as! WelcomeViewController;
                self.navigationController?.pushViewController(welcomeViewController, animated: true);
                self.dismiss(animated: true, completion: nil);
            }
        }
    }
}


extension String {
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
