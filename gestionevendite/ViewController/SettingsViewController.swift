//
//  SettingsViewController.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 11/03/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit
import MessageUI


class SettingsViewController: UIViewController, MFMailComposeViewControllerDelegate {

    
    @IBOutlet weak var buttonInviaEmail: UIButton!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonMenu: UIBarButtonItem!
    @IBOutlet weak var switchTouchId: UISwitch!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        buttonInviaEmail.layer.borderColor = UIColor.white.cgColor;
        buttonInviaEmail.layer.borderWidth = 1.0
        buttonInviaEmail.layer.cornerRadius = 5.0
        
        buttonCall.layer.borderColor = UIColor.white.cgColor;
        buttonCall.layer.borderWidth = 1.0
        buttonCall.layer.cornerRadius = 5.0
    }
    
    @IBAction func showMenu(_ sender: Any) {
        panel?.openLeft(animated: true)
    }
    
    
    @IBAction func sentEmail(_ sender: Any) {
        let alert = AJAlertController.initialization();
        let messaggio = "Vuoi inviare una email al supporto ?";
        alert.showAlert(aStrMessage: messaggio, aCancelBtnTitle: "NO", aOtherBtnTitle: "SI", completion: {
            (index, title) in
            if(index == 1){
                print("Devo inviare l'email");
                if MFMailComposeViewController.canSendMail(){
                    
                    let mailComposer = MFMailComposeViewController();
                    mailComposer.mailComposeDelegate = self;
                    mailComposer.setToRecipients(["vincenzo.scaccia@gmail.com"]);
                    mailComposer.setSubject("Errore");
                    mailComposer.setMessageBody("Test invio email", isHTML: true);
                    
                    self.present(mailComposer, animated:true, completion:nil);
                }
            }
        })
    }
    
    @IBAction func callPhone(_ sender: Any) {
        
        let alert = AJAlertController.initialization();
        let messaggio = "Sei sicuro di voler chiamare il supporto ?";
        alert.showAlert(aStrMessage: messaggio, aCancelBtnTitle: "NO", aOtherBtnTitle: "SI", completion:
            { (index, title) in
                if(index == 1){
                    print("Devo fare la chiamata");
                    let url: NSURL = URL(string: "TEL://+393515084883") as! NSURL;
                    UIApplication.shared.open(url as URL
                        , options: [:], completionHandler: nil);
                }
            }
        );
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func enableTouchId(_ sender: Any) {
        if(switchTouchId.isOn){
            let storyBoard = UIStoryboard(name: "Main", bundle: nil);
            let modalViewController = storyBoard.instantiateViewController(withIdentifier: "ModalTouchId");
            self.present(modalViewController, animated:true, completion:nil);
        }
    }
    
}
