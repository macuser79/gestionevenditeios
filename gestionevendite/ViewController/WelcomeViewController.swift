//
//  WelcomeViewController.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 06/03/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit
import Alamofire
import APESuperHUD
import SwiftyJSON
import RealmSwift

class WelcomeViewController: UIViewController {

    
    @IBOutlet weak var username: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageButtonBack = UIImage(named: "menu");
        let backButton = UIBarButtonItem(image: imageButtonBack, style: .plain, target: self, action: nil);
        backButton.tintColor = UIColor.white;
        backButton.action = #selector(buttonClicked(sender:));
        navigationItem.leftBarButtonItem = backButton;
        
        let defaultValues = UserDefaults.standard;
        var sUsername : String! = "";
        
        if !isBlank(optionalString: defaultValues.string(forKey: "nome")){
            sUsername = "\(String(describing: defaultValues.string(forKey: "nome")))"
        }
        
        if !isBlank(optionalString: defaultValues.string(forKey: "cognome")){
            sUsername = sUsername+" \(String(describing: defaultValues.string(forKey: "cognome")))";
        }
        
        if !isBlank(optionalString: defaultValues.string(forKey: "denominazione")){
            sUsername = "\(String(describing: defaultValues.string(forKey: "denominazione")))";
        }
        username.text = sUsername;
        

        let isPreloaded = defaultValues.bool(forKey: "isPreloaded");
        if(!isPreloaded){
            self.preloadData();
            defaultValues.setValue(true, forKey: "isPreloaded");
        }
//        else{
//            defaultValues.setValue(false, forKey: "isPreloaded");
//        }
    }
    
    @IBAction func logouButton(_ sender: Any) {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!);
        UserDefaults.standard.synchronize();
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController;
        self.navigationController?.pushViewController(loginVC, animated: true);
        
    }
    
    @objc func buttonClicked(sender: UIBarButtonItem) {
        panel?.openLeft(animated: true)
    }

    
    func isBlank (optionalString :String?) -> Bool {
        if let string = optionalString {
            return string.isEmpty
        } else {
            return true
        }
    }
    
    
    func preloadData(){
        let hudViewController = APESuperHUD(style: .loadingIndicator(type: .standard), title: nil, message: "Loading...")
        present(hudViewController, animated: true)
        let realm = try! Realm()
        Alamofire.SessionManager.default.request(ClientRouter.getAllClient).responseJSON{
            response in
            if let jsonReturn = response.result.value{
                let jsonData = JSON(jsonReturn);
                for (index,subJson):(String, JSON) in jsonData {
                    
                    let cliente = Client();
                    cliente.cap = subJson["cap"].stringValue;
                    cliente.citta = subJson["citta"].stringValue;
                    cliente.clienteId = subJson["clienteId"].intValue;
                    cliente.email = subJson["email"].stringValue;
                    cliente.fax = subJson["fax"].stringValue;
                    cliente.flgAcconto = subJson["flgAcconto"].intValue;
                    cliente.identificativo = subJson["identificativo"].stringValue;
                    cliente.idListino = subJson["idListino"].intValue;
                    cliente.indirizzo = subJson["indirizzo"].stringValue;
                    cliente.note = subJson["note"].stringValue;
                    cliente.orario = subJson["orario"].stringValue;
                    cliente.piva = subJson["piva"].stringValue;
                    cliente.provincia = subJson["provincia"].stringValue;
                    cliente.ragioneSociale = subJson["ragioneSociale"].stringValue;
                    cliente.rivendita = subJson["rivendita"].stringValue;
                    try! realm.write{
                        realm.add(cliente);
                        print("Cliete aggiunto")
                    }
                }
                hudViewController.dismiss(animated: true);
            }
        }
    }
    
}
