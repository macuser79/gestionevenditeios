//
//  OrderTableViewController.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 19/11/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit
import Alamofire
import APESuperHUD
import SwiftyJSON

class OrderTableViewController: UITableViewController {
    
    
    var listOrder: NSMutableArray = []
    var refreshController = UIRefreshControl();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.refreshController.attributedTitle = NSAttributedString(string: "Aggiornamento ordini");
        //self.refreshController.addTarget(self, action:Selector("refresh:"), for: .valueChanged);
        //self.tableView.addSubview(refreshController);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadOrderByAgente(idAgente: "1");
    }
    
    
    @IBAction func showMenu(_ sender: Any) {
        panel?.openLeft(animated: true)
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.listOrder.count>0){
            return listOrder.count
        }else{
            return 0
        }
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellOrder", for: indexPath)
        //cell.textLabel?.text = "Pippo";
        let elementOrder = listOrder.object(at: indexPath.row) as! Order;
        cell.textLabel?.text = elementOrder.descOrder;
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func refresh(sender:AnyObject) {
        self.loadOrderByAgente(idAgente: "1");
    }
    
    
    
    func loadOrderByAgente(idAgente: String){
        let hudViewController = APESuperHUD(style: .loadingIndicator(type: .standard), title: nil, message: "Loading...")
        present(hudViewController, animated: true)
        Alamofire.SessionManager.default.request(OrdineRouter.getAllOrderByIdAgente(idAgente: "1")).responseJSON{
            response in
                if let JSONData = response.result.value{
                    print("Valore jsonDATA\(JSONData)")
                    let jsonData = JSON(JSONData);
                    for (__,subJson):(String, JSON) in jsonData {
                        //print("Element \(subJson["descCliente"].stringValue)");
                        let order = Order();
                        order.descClient = subJson["descCliente"].stringValue;
                        order.descStato = subJson["descStato"].stringValue;
                        order.id = subJson["id"].stringValue
                        order.idClient = subJson["idCliente"].stringValue;
                        order.idOrder = subJson["idOrder"].stringValue;
                        order.idStato = subJson["idStato"].stringValue;
                        order.prezzo = subJson["prezzo"].stringValue;
                        order.quantita = subJson["quantita"].stringValue;
                        order.descOrder = subJson["descOrder"].stringValue;
                        order.dataOrdine = subJson["dataOrdine"].stringValue;
                        self.listOrder.add(order);
                    }
                    print("Numero elementi: \(self.listOrder.count)");
                    self.tableView.reloadData();
                    hudViewController.dismiss(animated: true);
            }
        }
    }
    
    
    
    
}
