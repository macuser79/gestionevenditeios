//
//  ModalTouchIdViewController.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 12/03/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit

class ModalTouchIdViewController: UIViewController {
    
    
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var buttonConfirm: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonConfirm.layer.borderColor = UIColor.white.cgColor;
        buttonConfirm.layer.borderWidth = 1.0
        buttonConfirm.layer.cornerRadius = 5.0
        self.hideKeyboardWhenTappedAround();
        
        let bottomBorder = UIView();
        bottomBorder.backgroundColor = UIColor.white
        bottomBorder.frame = CGRect(x: 0, y: textPassword.frame.height - 1, width: textPassword.frame.width, height:1);
        textPassword.addSubview(bottomBorder);
        
        
    }
    

    @IBAction func confirmAction(_ sender: Any) {
        dismiss(animated: true, completion:nil);
    }

}

