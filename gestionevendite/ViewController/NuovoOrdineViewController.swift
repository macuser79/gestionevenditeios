//
//  NuovoOrdineViewController.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 19/04/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit
import RealmSwift

class NuovoOrdineViewController: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var buttonDataOrdine: UIButton!
    @IBOutlet weak var buttonProdotti: UIButton!
    @IBOutlet weak var sendOrder: UIButton!
    @IBOutlet weak var txtExample: UITextField!
    @IBOutlet weak var pickerCliente: UIPickerView!
    @IBOutlet weak var tabBarButton: UIToolbar!
    @IBOutlet weak var buttonDone: UIBarButtonItem!
    @IBOutlet weak var txtDataOrdine: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    
    
    var pickerData : Results<Client>!;
    let realm = try! Realm();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapHiddenPicker = UITapGestureRecognizer(target: self, action: #selector(NuovoOrdineViewController.hiddenPicker(_:)))
        view.addGestureRecognizer(tapHiddenPicker);
        
        self.setupUITxtCliente();
        self.setupUITextData();
        self.loadPicker();
        pickerCliente.dataSource = self;
        pickerCliente.delegate = self;
        
        
        
        
    }
    
    @IBAction func showMenu(_ sender: Any) {
        panel?.openLeft(animated: true)
    }
    
    
    @IBAction func dismissPicker(_ sender: Any) {
        datePicker.isHidden = true;
        pickerCliente.isHidden = true;
        tabBarButton.isHidden = true;
        
    }
    
    
    
    //MARK - Disegno l'interfaccia per la prima casella di testo
    func setupUITxtCliente(){
        let imageClient = UIImage(named: "txt_icon_client");
        let imageView = UIImageView(frame: CGRect(x:0,y:0, width: 25, height: 25));
        imageView.image = imageClient;
        txtExample.rightView = imageView;
        txtExample.rightViewMode = .always;
        txtExample.addBottomBorder();
        let tapShowData = UITapGestureRecognizer(target: self, action: #selector(NuovoOrdineViewController.myviewTapped(_:)))
        tapShowData.delegate = self;
        txtExample.addGestureRecognizer(tapShowData);
        txtExample.delegate = self;
    }
    
    //datePicker.datePickerMode = .date;
    func setupUITextData(){
        let imageClient = UIImage(named: "txt_icon_calendar");
        let imageView = UIImageView(frame: CGRect(x:0,y:0, width: 25, height: 25));
        imageView.image = imageClient;
        txtDataOrdine.rightView = imageView;
        txtDataOrdine.rightViewMode = .always;
        txtDataOrdine.addBottomBorder();
        let tapShowData = UITapGestureRecognizer(target: self, action: #selector(NuovoOrdineViewController.myViewDate(_:)))
        tapShowData.delegate = self;
        txtDataOrdine.addGestureRecognizer(tapShowData);
        txtDataOrdine.delegate = self;
        datePicker.backgroundColor = .lightGray;
        datePicker.addTarget(self, action: #selector(NuovoOrdineViewController.changeDate(_:)), for: .valueChanged);
    }
    
    
    
    
    func loadPicker(){
        pickerData = realm.objects(Client.self);
        pickerCliente.isHidden = true;
        tabBarButton.isHidden = true;
        datePicker.isHidden = true;
    }
    
    
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        pickerCliente.isHidden = false;
        tabBarButton.isHidden = false;
        datePicker.isHidden = true;
    }
    
    
    @objc func myViewDate(_ sender: UITapGestureRecognizer){
        datePicker.isHidden = false;
        tabBarButton.isHidden = false;
        pickerCliente.isHidden = true;
    }
    
    
    @objc func hiddenPicker(_ sender: UITapGestureRecognizer){
        pickerCliente.isHidden = true;
        tabBarButton.isHidden = true;
        datePicker.isHidden = true;
    }
    
    @objc func changeDate(_ sender: UIDatePicker){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        txtDataOrdine.text = formatter.string(from: datePicker.date);
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtExample {
            return false; //do not show keyboard nor cursor
        }
        if(textField == txtDataOrdine){
            return false;
        }
        return false;
    }

}

//MARK - DataPickerview Delegate e DataSource
extension NuovoOrdineViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let elementClient = pickerData![row];
        return elementClient.ragioneSociale;
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let elementClient = pickerData![row];
        txtExample.text = elementClient.ragioneSociale;
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let elementClient = pickerData![row];
        let labelElement = UILabel();
        labelElement.textColor = .darkGray;
        labelElement.text = elementClient.ragioneSociale;
        labelElement.textAlignment = .center;
        return labelElement;
    }

}


