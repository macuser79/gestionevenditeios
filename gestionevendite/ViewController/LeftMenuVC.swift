//
//  LeftMenuVC.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 07/03/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit

class LeftMenuVC: UIViewController {

    @IBOutlet weak var tableMenu: UITableView!
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var imageUser: UIImageView!
    
    var menus = [MenuModel]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let menuCreaOrdine = MenuModel(idMenu: 1, imageMenu: "ordine", titleMenu:"Nuovo ordine");
        let menuListaOrdine = MenuModel(idMenu: 2, imageMenu: "ordine", titleMenu: "Lista ordini")
        let menuSetting = MenuModel(idMenu: 3, imageMenu: "settings", titleMenu:"Impostazioni")
        menus.append(menuCreaOrdine);
        menus.append(menuListaOrdine);
        menus.append(menuSetting);
        tableMenu.alwaysBounceVertical = false
    }
}


extension LeftMenuVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menus.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath);
        let menuItem = self.menus[indexPath.row];
        cell.textLabel?.text = menuItem.titleMenu;
        cell.imageView?.image = UIImage(named: menuItem.imageMenu!);
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if(indexPath.row == 0){
            let nuovoOrdineVC = mainStoryboard.instantiateViewController(withIdentifier: "nuovoOrdineVC") as! NuovoOrdineViewController
            let centerNavVC = UINavigationController(rootViewController: nuovoOrdineVC);
            panel!.center(centerNavVC);
        }
        else if(indexPath.row == 1){
            let listaOrdiniVC = mainStoryboard.instantiateViewController(withIdentifier: "listOrdineVC") as!
                OrderTableViewController
            let centerNavVC = UINavigationController(rootViewController: listaOrdiniVC);
            panel!.center(centerNavVC);
        }
        
        
        if(indexPath.row == 2){
            let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "settingsVC") as! SettingsViewController
            let centerNavVC = UINavigationController(rootViewController: settingsVC);
            panel!.center(centerNavVC);
        }
    }
}
