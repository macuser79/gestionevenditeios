//
//  OrdineRouter.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 19/11/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import Foundation
import Alamofire

enum OrdineRouter: URLRequestConvertible {
    
    case getAllOrderByIdAgente(idAgente: String)
    case getSingleOrder(idOrder: String)
    
    static let baseURLString = "http://127.0.0.1:8080/GestioneVendite/webservices/GestioneVenditeWebServices"
    
    var method: HTTPMethod {
        switch self {
        case .getAllOrderByIdAgente:
            return .get
        case .getSingleOrder:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getAllOrderByIdAgente(let idAgente):
            return "/listOrdineByAgente/\(idAgente)";
        case .getSingleOrder(let idOrder):
            return "/listOrderById/\(idOrder)";
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try UserRouter.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        print("Valore url \(String(describing: urlRequest.url?.absoluteString))")
        return urlRequest
    }
}
    
