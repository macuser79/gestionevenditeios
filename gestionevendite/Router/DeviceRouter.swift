//
//  DeviceRouter.swift
//  MyHoroscopo
//
//  Created by Vincenzo Scaccia on 22/02/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit
import Alamofire


enum DeviceRouter : URLRequestConvertible{
    
    case registerDevice(deviceToken: String, udid: String, typeDevice: String)
    case updateDevice(deviceToken: String, udid: String, typeDevice: String)
    
    
    static let baseURLString = "http://10.100.116.115:8080/OroscopoClient/rest"
    
    var method: HTTPMethod {
        switch self {
        case .registerDevice:
            return .put
        case .updateDevice:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .registerDevice(let deviceToken, let udidDevice, let typeDevice):
            return "/deviceClient/\(deviceToken)/\(udidDevice)/\(typeDevice)";
        case .updateDevice(let deviceToken, let udid, let typeDevice):
            return "";
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try DeviceRouter.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        print("Valore url \(urlRequest.url?.absoluteString)")
        return urlRequest
    }
}

