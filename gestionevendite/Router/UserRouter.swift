//
//  UserRouter.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 06/03/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import UIKit
import Alamofire

enum UserRouter: URLRequestConvertible {
    
    case registerUser(deviceToken: String, udid: String, typeDevice: String)
    case loginUser(username: String, password: String)

    static let baseURLString = "http://127.0.0.1:8080/GestioneVendite/webservices/GestioneVenditeWebServices"
    //static let baseURLString = "http://51.75.66.217:8080/GestioneVendite/webservices/GestioneVenditeWebServices"
    
    var method: HTTPMethod {
        switch self {
        case .registerUser:
            return .put
        case .loginUser:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .registerUser(let deviceToken, let udidDevice, let typeDevice):
            return "/deviceClient/\(deviceToken)/\(udidDevice)/\(typeDevice)";
        case .loginUser(let username, let password):
            return "/checkUserApp/\(username)/\(password)";
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try UserRouter.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        print("Valore url \(String(describing: urlRequest.url?.absoluteString))")
        return urlRequest
    }
}
