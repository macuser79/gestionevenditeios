//
//  ClientRouter.swift
//  gestionevendite
//
//  Created by Vincenzo Scaccia on 20/04/2019.
//  Copyright © 2019 Vincenzo Scaccia. All rights reserved.
//

import Foundation
import Alamofire

enum ClientRouter: URLRequestConvertible {
    
    case getAllClient
    
    //static let baseURLString = "http://51.75.66.217:8080/GestioneVendite/webservices/GestioneVenditeWebServices"
    static let baseURLString = "http://127.0.0.1:8080/GestioneVendite/webservices/GestioneVenditeWebServices"

    var method: HTTPMethod {
        switch self {
        case .getAllClient:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getAllClient:
            return "/listCliente";
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ClientRouter.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        return urlRequest
    }
    
    
}
